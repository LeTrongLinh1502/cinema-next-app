import React, { FC } from "react";
import { Link } from "react-scroll";
type Props = {
  title: string;
  name: string;
  isDynamic: boolean;
  onClick?: () => void;
};

const NavItem: FC<Props> = ({ title, name, isDynamic, onClick }) => {
  const content = (
    <div className="cursor-pointer tracking-wide shrink-0 relative group select-none">
      <span className="nav-text md:group-hover:text-shadow-bold">{title}</span>
      <span className="nav-highlight w-5/6 h-1 mx-auto bg-secondary-yellow absolute transition-all left-0 right-0 bottom-[-6px] invisible" />
    </div>
  );
  const linkProps = isDynamic
    ? {
        activeClass: "active",
        spy: true,
        smooth: true,
        duration: 500,
        className: `${name} nav`,
      }
    : {};

  return (
    <li onClick={onClick}>
      {isDynamic ? (
        <Link to={name} {...linkProps}>
          {content}
        </Link>
      ) : (
        content
      )}
    </li>
  );
};

export default NavItem;
