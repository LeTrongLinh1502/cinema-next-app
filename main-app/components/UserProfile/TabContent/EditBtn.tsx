import React, { memo } from "react";
import PenIcon from "@/assets/Profile/Pen.svg";

type Props = {
  onEdit: () => void;
  onReset: () => void;
  isSubmit: boolean;
  isEdit: boolean;
};
const EditBtn = ({ onEdit, onReset, isSubmit, isEdit }: Props) => {
  return (
    <div className="flex justify-end gap-2 mt-6">
      {!isEdit && (
        <button
          className="btn btn-sm hover:bg-secondary-yellow bg-secondary-yellow text-title-dark flex gap-2"
          onClick={onEdit}
        >
          <PenIcon width={14} height={14} />
          <span>Edit Profile</span>
        </button>
      )}
      {isEdit && (
        <>
          <button
            type="reset"
            onClick={onReset}
            className="btn btn-sm hover:bg-secondary-yellow bg-secondary-yellow text-title-dark"
          >
            Cancel
          </button>
          <button
            type="submit"
            className={`btn btn-sm hover:bg-secondary-yellow bg-secondary-yellow text-title-dark ${
              isSubmit ? "loading" : ""
            }`}
          >
            Save Changes
          </button>
        </>
      )}
    </div>
  );
};

export default memo(EditBtn);
