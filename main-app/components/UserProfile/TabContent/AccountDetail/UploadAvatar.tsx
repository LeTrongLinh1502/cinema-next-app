import { useRef, useState, useCallback, useEffect } from "react";
import Dropzone, { FileRejection } from "react-dropzone";
import AvatarEditor from "react-avatar-editor";
import Modal from "@/components/Utils/Modal";
import UploadIcon from "@/assets/Profile/upload-image.svg";
import RotateIcon from "@/assets/Profile/Rotate.svg";
import { ref, uploadString, getDownloadURL } from "firebase/storage";
import { auth, storage } from "firebaseConfig";
import { updateProfile } from "firebase/auth";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastOptions } from "@/utils/toastConfig";

type Props = {
  onClose: () => void;
  onUploadSuccess?: () => void;
};

const UploadAvatar = ({ onClose, onUploadSuccess }: Props) => {
  const [img, setImg] = useState<File | undefined>();
  const [scale, setScale] = useState(1);
  const [deg, setDeg] = useState(0);
  const [isUploading, setIsUploading] = useState(false);

  // get image width and height
  const editorRef = useRef<AvatarEditor>(null);

  const handleDrop = useCallback((acceptedFiles: File[]) => {
    if (acceptedFiles.length > 0) {
      setImg(acceptedFiles[0]);
    }
  }, []);

  const handleRotate = () => {
    setDeg((deg) => {
      const newDeg = deg + 90;
      return newDeg > 360 ? 0 : newDeg;
    });
  };

  const handleReject = () => {
    toast.error(
      "The image file upload size limit is 4 megabytes",
      toastOptions
    );
  };

  const handleClose = () => {
    onClose();
  };

  useEffect(() => {
    // reset rotate after choose new image
    setDeg(0);
  }, [img]);

  const handleSave = async () => {
    const user = auth.currentUser;
    if (!user) return;
    if (!editorRef.current) {
      return;
    }
    try {
      setIsUploading(true);
      const canvas = editorRef.current.getImage();
      const dataUrl = canvas.toDataURL();
      const storageRef = ref(storage, `avatars/${user.uid}.png`);
      await uploadString(storageRef, dataUrl, "data_url", {
        contentType: "image/png",
      });
      const downloadUrl = await getDownloadURL(storageRef);
      await updateProfile(user, { photoURL: downloadUrl });

      onUploadSuccess && onUploadSuccess();
      toast.success("Update avatar successfully", toastOptions);
      onClose();
    } catch (error) {
      toast.error("Upload avatar failed", toastOptions);
      setImg(undefined);
      console.log(error);
    } finally {
      setIsUploading(false);
    }
  };

  const isHasFile = img instanceof File;

  return (
    <>
      <ToastContainer />
      <Modal
        onCloseModal={handleClose}
        modalName="Change Avatar"
        className="bg-[#232323] max-w-[700px]"
      >
        <div className="w-full h-full mt-8 px-8 flex flex-col select-none">
          <Dropzone
            maxSize={8388608}
            onDropAccepted={handleDrop}
            onDropRejected={handleReject}
            accept="image/*"
            maxFiles={1}
            noClick={isHasFile}
          >
            {({ getRootProps, getInputProps, open }) => (
              <>
                <div
                  {...getRootProps({
                    style: {
                      display: "flex",
                      height: "100%",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                      color: "#bdbdbd",
                      border: "2px dashed #8c8c8c",
                      borderRadius: "10px",
                    },
                  })}
                >
                  {img && (
                    <AvatarEditor
                      height={280}
                      width={280}
                      image={img}
                      scale={scale}
                      borderRadius={99999}
                      style={{
                        background: "#3f3f3f",
                      }}
                      rotate={deg}
                      ref={editorRef}
                    />
                  )}
                  <input {...getInputProps()} />
                  {!img && (
                    <div className="flex flex-col text-white items-center gap-1">
                      <UploadIcon width="80" height="80" />
                      <span className="font-bold text-2xl">
                        Select an image to upload
                      </span>
                      <p className="text-gray-light">
                        or drag and drop it here
                      </p>
                    </div>
                  )}
                </div>
                <div className="flex my-6 items-center gap-4">
                  <div className="flex items-center gap-4 basis-1/2">
                    <span className="text-3xl">-</span>
                    <input
                      type="range"
                      min="100"
                      max="300"
                      defaultValue={0}
                      className="range range-xs"
                      onChange={(e) => setScale(Number(e.target.value) / 100)}
                    />
                    <span className="text-3xl">+</span>
                  </div>
                  <div
                    className="cursor-pointer p-1 mt-1"
                    onClick={handleRotate}
                  >
                    <RotateIcon width="24" height={24} />
                  </div>
                </div>
                <div className="flex gap-4 self-end">
                  {isHasFile && (
                    <button
                      onClick={open}
                      className="btn btn-sm btn-outline px-4 normal-case"
                    >
                      Choose another ...
                    </button>
                  )}
                  <button
                    onClick={handleSave}
                    className={`btn btn-sm bg-secondary-yellow border-secondary-yellow normal-case self-end text-gray-dark hover:bg-secondary-yellow border-none ${
                      isUploading ? "loading" : ""
                    }`}
                  >
                    Confirm upload
                  </button>
                </div>
              </>
            )}
          </Dropzone>
        </div>
      </Modal>
    </>
  );
};

export default UploadAvatar;
