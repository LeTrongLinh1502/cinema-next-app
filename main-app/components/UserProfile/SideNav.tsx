import React from "react";
type Tab = {
  name: string;
  code: string;
};
type Props = {
  tabs: Tab[];
  selectedTab: Tab;
  onChangeTab: (clickedTab: Tab) => void;
};
const SideNav = ({ tabs, selectedTab, onChangeTab }: Props) => {
  return (
    <ul className="flex flex-col gap-4 items-start">
      {tabs.map((tab, index) => (
        <li
          key={index}
          onClick={() => onChangeTab(tabs[index])}
          className={`cursor-pointer font-medium ${
            tab.code === selectedTab.code
              ? "text-secondary-yellow"
              : "text-gray-light"
          }`}
        >
          {tab.name}
        </li>
      ))}
    </ul>
  );
};

export default SideNav;
