import { Tab } from "@/types/profile";
import { useAuthUser } from "@react-query-firebase/auth";
import { auth } from "firebaseConfig";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import UserAvatar from "../Utils/UserAvatar";
import SideNav from "./SideNav";
import TabContent from "./TabContent/TabContent";
import "react-loading-skeleton/dist/skeleton.css";
import useAuth from "@/hooks/useAuth";
const tabsInitial: Tab[] = [
  { name: "Account Details", code: "DETAIL" },
  { name: "Transaction History", code: "TRANSACTION" },
];

const UserProfile = () => {
  const [tabs, setTabs] = useState<Tab[]>(tabsInitial);
  const [selectedTab, setSelectedTab] = useState<Tab>(tabs[0]);

  const user = useAuth();

  const { providerId } = user.data?.providerData?.[0] || {};
  const { isLoading, data } = user;
  useEffect(() => {
    if (providerId === "password") {
      setTabs((tabs) => [
        ...tabsInitial,
        { name: "Change Password", code: "PASSWORD" },
      ]);
    }
  }, [providerId]);

  const handleChangeTab = (clickedTab: Tab) => {
    if (selectedTab.code === clickedTab.code) {
      return;
    }
    setSelectedTab(clickedTab);
  };

  return (
    <div className="w-full flex flex-col py-16 px-24 items-center">
      <div className="flex flex-col gap-12 w-[70%]">
        <div className="w-full flex gap-5 items-center">
          <UserAvatar
            photoURL={data?.photoURL}
            displayName={data?.displayName}
            size={60}
          />
          <div className="flex flex-col">
            <h2 className="font-bold text-2xl">
              <span>
                {data?.displayName
                  ? `${data?.displayName} / ${selectedTab.name}`
                  : "Loading..."}
              </span>
            </h2>
            <p className="text-gray-light text-[17px]">
              Set up your personal information
            </p>
          </div>
        </div>
        <div className="flex">
          <div className="flex flex-col text-[17px] basis-1/3">
            <SideNav
              tabs={tabs}
              selectedTab={selectedTab}
              onChangeTab={handleChangeTab}
            />
          </div>
          <TabContent selectedTab={selectedTab} />
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
