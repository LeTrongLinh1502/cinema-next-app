import { TFunction } from "next-i18next";
import { FC } from "react";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import PageWrapper from "../Utils/PageWrapper";

type Props = {
  t: TFunction;
};
const Layout: FC<Props> = ({ t, children }) => {
  return (
    <PageWrapper>
      <Header t={t} />
      {children}
      <Footer />
    </PageWrapper>
  );
};

export default Layout;
