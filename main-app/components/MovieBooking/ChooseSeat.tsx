import { Seat } from "@/types/movieBooking";
import React from "react";
import useAuth from "@/hooks/useAuth";
import {
  collection,
  doc,
  getDoc,
  getDocs,
  limit,
  query,
  where,
  addDoc,
} from "firebase/firestore";
import { useAppSelector } from "@/redux/store";
import { db } from "firebaseConfig";
import { toast, ToastContainer } from "react-toastify";
import { toastOptions } from "@/utils/toastConfig";
import { useRouter } from "next/router";
import "react-toastify/dist/ReactToastify.css";
type SeatByType = {
  seatType: string;
  seats: Seat[];
};

type Props = {
  seatsByType: SeatByType[];
};

const ChooseSeat = ({ seatsByType }: Props) => {
  const seatsCount = seatsByType.reduce((acc, curr) => {
    return acc + curr.seats.length;
  }, 0);

  if (seatsCount === 0) {
    return (
      <div className="flex flex-col gap-2">
        <h3 className="font-bold">Choose seat</h3>
        <p className="text-gray-light">
          You haven&apos;t selected any seats yet
        </p>
      </div>
    );
  }
  return (
    <div className="flex flex-col gap-4">
      <h3 className="font-bold">Choose seat</h3>
      {seatsByType.map(({ seatType, seats }, idx) => {
        const totalByType = seats.reduce((acc, seat) => {
          const price = seat.price ? seat.price : seat.type.price;
          return acc + price;
        }, 0);

        return (
          <>
            <ToastContainer />
            <div
              key={idx}
              className="flex flex-col gap-2 border-2 border-gray-light py-2 px-3 rounded-lg"
            >
              <div className="flex gap-4">
                <span className="font-bold capitalize">{seatType}:</span>
                <ul className="flex gap-2 flex-wrap">
                  {seats.map((seat, idx) => {
                    return <li key={idx}>{seat.id}</li>;
                  })}
                </ul>
              </div>
              <div
                className="flex justify-end gap-4 items-center font-bold"
              >
                <span className="text-gray-light">x {seats.length}</span>
                <span className="">${totalByType}</span>
              </div>
            </div>
          </>
        );
      })}
    </div>
  );
};

export default ChooseSeat;
