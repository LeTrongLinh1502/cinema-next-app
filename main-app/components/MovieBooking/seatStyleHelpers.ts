import { SeatType, Status } from "../../types/movieBooking";

export const calculateSeatBoxWidth = (
  maxSlot: number,
  divWidth: number,
  gridGap: number
) => {
  const marginX = 24 * 2;
  const totalGap = gridGap * 4 * (maxSlot - 1);
  let boxSize = (divWidth - marginX - totalGap) / maxSlot;
  if (boxSize < 20) {
    boxSize = 20;
  }
  if (boxSize > 50) {
    boxSize = 50;
  }
  return Math.trunc(boxSize);
};

export const getSeatTypeStyle = (type: SeatType) => {
  switch (type.id) {
    case "standard":
      return "border-2 border-gray-light";
    case "vip":
      return "border-2 border-[#30a6fe]";
    case "couple":
      return "border-2 border-[#fe308c]";
    case "deluxe":
      return "border-2 border-[#feb308]";
    default:
      return "border-2 border-gray-light";
  }
};

export const getSeatStatusStyle = (status: Status) => {
  switch (status) {
    case "AVAILABLE":
      return "";
    case "RESERVED":
      return "bg-gray-light";
    case "UNAVAILABLE":
      return "bg-gray-light";
  }
};
