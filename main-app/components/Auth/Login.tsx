import React, { useEffect } from "react";
import GoogleIcon from "/assets/Auth/Google.svg";
import FacebookIcon from "/assets/Auth/Facebook.svg";
import InputField from "../Utils/InputField";
import Radio from "./Radio";
import Image from "next/image";
import BackgroundImg from "/assets/Auth/Background2.jpg";
import { SubmitHandler, useForm } from "react-hook-form";
import { AuthFormValues } from "../../types/form";
import yup from "../../utils/validation/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import { auth, googleProvider, facebookProvider } from "firebaseConfig";
import {
  useAuthGetRedirectResult,
  useAuthSignInWithEmailAndPassword,
  useAuthSignInWithPopup,
  useAuthSignInWithRedirect,
} from "@react-query-firebase/auth";
import { toast, ToastContainer } from "react-toastify";
import { toastOptions } from "@/utils/toastConfig";
import { useRouter } from "next/router";
import { getRedirectResult } from "firebase/auth";
import "react-toastify/dist/ReactToastify.css";
import { createUserIfNotExists, replaceUser } from "@/utils/firebase";
import { useAppDispatch } from "@/redux/store";
import { authActions } from "@/redux/reducers/authReducer";

const LoginSchema = yup.object({
  email: yup
    .string()
    .required("Please enter a valid email")
    .email("Please enter a valid email"),

  password: yup
    .string()
    .required("Password is required")
    .min(8, "Password must be at least 8 characters")
    .max(64, "Password must be less than 64 characters")
    .password("Your password must contains at least one letter and one number"),
});

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AuthFormValues>({
    mode: "onTouched",
    resolver: yupResolver(LoginSchema),
  });

  const router = useRouter();

  const passwordAuthMutation = useAuthSignInWithEmailAndPassword(auth, {
    onError(error) {
      if (
        error.code === "auth/user-not-found" ||
        error.code === "auth/wrong-password"
      ) {
        toast.error("Invalid email or password", toastOptions);
      } else toast.error(error.message, toastOptions);
    },
    onSuccess(data) {
      toast.success("Sign in successfully");
      router.push("/");
    },
  });

  const handleClickSignUp = () => {
    router.push("/signup");
  };

  const handleClickLogo = () => {
    router.push("/");
  };

  function onSignIn(email: string, password: string) {
    passwordAuthMutation.mutate({ email, password });
  }

  const authMutation = useAuthSignInWithPopup(auth, {
    onError(error) {
      if (error.code === "auth/popup-closed-by-user") {
        toast.error("Sign in has been cancelled", toastOptions);
      } else {
        toast.error(error.message, toastOptions);
      }
    },
    onSuccess(data) {
      toast.success("Sign in successfully", toastOptions);
      createUserIfNotExists(data.user);
      router.push("/");
    },
  });

  function onSignInWithGoogle() {
    authMutation.mutate({
      provider: googleProvider,
    });
  }
  const onSignInWithFacebook = () => {
    authMutation.mutate({
      provider: facebookProvider,
    });
  };

  const onSubmit: SubmitHandler<AuthFormValues> = (data) => {
    const { email, password } = data;
    onSignIn(email, password);
  };
  return (
    <>
      <ToastContainer />
      <div className="w-full min-h-screen h-full grid grid-cols-[1fr_max-content] relative">
        <div className="absolute left-10 top-4 z-10">
          <h1 onClick={handleClickLogo} className="font-bold cursor-pointer text-[40px]">Watchflix</h1>
        </div>

        <div className="w-full h-full relative">
          <h2 className="absolute z-20 text-6xl font-bold translate-x-[-50%] translate-y-[-50%] left-1/2 top-1/2 w-3/4 text-center">
            Discover new movies and buy tickets.
          </h2>
          <Image
            src={BackgroundImg}
            alt="Movie's cover"
            layout="fill"
            objectFit="cover"
          />
          <div className="absolute w-full h-full bg-[#00000049]"></div>
        </div>

        <div className="bg-[#161616] p-16 flex flex-col items-center justify-center z-20  max-w-[650px] w-full">
          <h1 className="font-bold text-3xl">Sign In to your account</h1>
          <span className="text-gray-light font-medium mt-2">
            &#8212; WITH &#8212;
          </span>

          <div className="grid grid-cols-2 gap-2 mt-6">
            <div
              onClick={() => onSignInWithGoogle()}
              className="border-radiants before:bg-auth-btn before:rounded-lg py-3 px-5 flex items-center gap-4 cursor-pointer"
            >
              <GoogleIcon width="24" height="24" />
              <span>Sign In with Google</span>
            </div>
            <div
              onClick={() => onSignInWithFacebook()}
              className="border-radiants before:bg-auth-btn before:rounded-lg py-3 px-5 flex items-center gap-4 cursor-pointer"
            >
              <FacebookIcon width="24" height="24" />
              <span>Sign In with Facebook</span>
            </div>
          </div>
          <span className="text-gray-light font-medium mt-6">
            &#8212; OR &#8212;
          </span>

          <form
            className="mt-4 flex flex-col self-stretch gap-4"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="flex flex-col gap-3">
              <InputField<AuthFormValues>
                label="Email"
                name="email"
                register={register}
                error={errors.email?.message}
                autoComplete="email"
              />
              <InputField<AuthFormValues>
                label="Password"
                name="password"
                type="password"
                register={register}
                error={errors.password?.message}
                autoComplete="password"
              />
            </div>
            <div className="flex justify-between mt-2 select-none">
              <div className="flex gap-3 ">
                <Radio />
                <span className="text-gray-light">Remember me</span>
              </div>
              <span className="text-gray-light cursor-pointer">
                Forgot Password?
              </span>
            </div>
            {passwordAuthMutation.isError && (
              <p>{passwordAuthMutation.error.message}</p>
            )}
            <button
              type="submit"
              className="bg-auth-btn py-3 rounded-lg font-medium text-lg mt-5 select-none"
            >
              Sign In to Your Account
            </button>
            <p className="mx-auto text-gray-light mt-5">
              Not a member yet?{" "}
              <span
                onClick={handleClickSignUp}
                className="underline text-typo-light font-bold cursor-pointer"
              >
                Sign Up Now
              </span>
            </p>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
