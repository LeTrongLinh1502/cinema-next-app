import React, { FC, memo } from "react";

type Props = {
  onClick?: React.MouseEventHandler;
  svgIcon?: any;
  loading: boolean;
  className?: string;
  children?: React.ReactNode;
};

const LoadingButton: FC<Props> = ({
  onClick,
  svgIcon,
  loading,
  children,
  className,
}) => {
  return (
    <button
      onClick={onClick}
      className={`btn text-primary-dark bg-secondary-yellow md:hover:bg-yellow-300 gap-1 ${
        loading ? "loading disabled" : ""
      } ${className ? className : ""}`}
    >
      {!loading && svgIcon ? svgIcon({ width: "18", height: "18" }) : ""}
      {children}
    </button>
  );
};

export default memo(LoadingButton);
