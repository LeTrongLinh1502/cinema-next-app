import React, { FC, useEffect } from "react";
import XIcon from "@/assets/Profile/X.svg";
type Props = {
  onCloseModal: () => void;
  onClickOverlay?: () => void;
  className?: string;
  modalName?: string;
  disableScroll?: boolean;
};

const Overlay = ({ onClick }: { onClick?: () => void }) => {
  return (
    <div
      onClick={onClick}
      className="fixed top-0 left-0 w-screen h-screen bg-black opacity-50 z-50"
    />
  );
};
const Modal: FC<Props> = ({
  onCloseModal,
  onClickOverlay,
  className,
  modalName,
  children,
  disableScroll = true,
}) => {
  useEffect(() => {
    if (disableScroll) {
      document.body.style.overflow = "hidden";
      return () => {
        document.body.style.overflow = "unset";
      };
    }
  }, [disableScroll]);
  return (
    <>
      <Overlay onClick={onClickOverlay} />
      <div className="fixed z-50 top-0 left-0 w-screen h-screen flex items-center justify-center">
        <div
          className={`relative rounded-3xl overflow-hidden flex flex-col z-50 w-3/5 h-4/5 px-8 py-8 ${
            className ? className : ""
          }`}
        >
          <button onClick={onCloseModal} className="absolute right-8 top-8">
            <XIcon width={20} height={20} />
          </button>
          {modalName && (
            <h1 className="font-bold text-2xl leading-none">{modalName}</h1>
          )}
          {children}
        </div>
      </div>
    </>
  );
};

export default Modal;
