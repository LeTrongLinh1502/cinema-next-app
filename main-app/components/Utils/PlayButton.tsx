import React, { ReactNode } from "react";
import PlayIcon from "/assets/Home/Play.svg";
import Image from "next/image";

type Props = {
  className?: string;
  hasOutline?: boolean;
};

const PlayButton = ({ className, hasOutline = false }: Props) => {
  return (
    <div
      className={`rounded-full relative flex justify-center items-center ${
        className || ""
      }`}
    >
      <button
        className={`lg:w-1/2 lg:h-1/2 bg-current rounded-full flex items-center justify-center cursor-pointer transition-all scale-90 md:hover:scale-100 peer z-10`}
      >
        <PlayIcon />
      </button>
      {hasOutline && (
        <span className="absolute top-0 block w-full h-full rounded-full border-[3px] border-current peer-hover:animate-bubble-slow"></span>
      )}
    </div>
  );
};

export default PlayButton;
