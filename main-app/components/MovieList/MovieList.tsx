import Link from "next/link";
import React from "react";
import { Element } from "react-scroll";
import MovieCard from "./MovieCard";
import { Movie } from "../../types/movie";
import { TFunction } from "next-i18next";
import { useRouter } from "next/router";

type Props = {
  title: string;
  name: string;
  movies: Movie[];
  t: TFunction;
};

const MovieList = ({ title, name, movies, t }: Props) => {
  const router = useRouter();

  if (movies.length === 0) {
    return (
      <div className="w-full h-full flex justify-center items-center">
        <h2 className="text-2xl font-bold">No Movies Found</h2>
      </div>
    );
  }
  console.log(movies);

  const routeToMovieDetail = (movieId: string) => {
    router.push(`/movies/${movieId}`);
  };

  const featuredMovie = movies[0];

  return (
    <Element name={name} className="element">
      <div className="w-5/6 h-[1300px] flex flex-col py-28 px-24 mx-auto">
        <div className="w-full flex justify-between items-center mb-6">
          <h2 className="font-medium text-4xl">{title}</h2>
          <Link href="#">
            <a className="text-secondary-yellow text-xl font-bold">
              {t("default:viewmore")}
            </a>
          </Link>
        </div>
        <div className="flex-1 w-full grid grid-cols-3 grid-rows-2 gap-4">
          <div className="col-span-2">
            <MovieCard
              id={featuredMovie.id}
              title={featuredMovie.title}
              imageUrl={featuredMovie.coverUrl}
              genres={featuredMovie.genres}
              duration={featuredMovie.duration}
              ratingPath={featuredMovie.ratingPath}
              onClick={() => routeToMovieDetail(featuredMovie.urlId as string)}
            />
          </div>

          {movies.slice(1).map((movie) => (
            <div key={movie.id} id={movie.id} className="col-span-1">
              <MovieCard
                id={movie.id}
                title={movie.title}
                imageUrl={movie.imageUrl}
                genres={movie.genres}
                duration={movie.duration}
                ratingPath={movie.ratingPath}
                onClick={() => routeToMovieDetail(movie.urlId as string)}
              />
            </div>
          ))}
        </div>
      </div>
    </Element>
  );
};

export default MovieList;
