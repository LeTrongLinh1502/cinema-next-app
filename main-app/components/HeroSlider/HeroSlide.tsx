import React, { FC } from "react";
import Image from "next/image";
import { Movie } from "../../types/movie";
import PlayButton from "../Utils/PlayButton";
import { useRouter } from "next/router";
type Props = {
  urlId: string;
  coverUrl: string;
  title: string;
  description: string;
  genres: string[];
  duration: number;
};

const HeroSlide: FC<Props> = (props) => {
  const router = useRouter();
  const handleBuyTicket = () => {
    router.push(`/movie/${props.urlId}`);
  };
  const durationStr = `${props.duration} min`;
  return (
    <div className="flex w-full h-full py-24 px-24 relative bg-hero-layout">
      <div className="absolute w-full h-full top-0 left-0 -z-10">
        <Image
          src={props.coverUrl}
          alt={`${props.title}'s cover`}
          layout="fill"
          objectFit="cover"
          objectPosition={`left top`}
          priority={true}
        />
      </div>
      <div className="flex-1 self-end">
        <PlayButton
          className="lg:w-36 lg:h-36 text-secondary-yellow"
          hasOutline={true}
        />
      </div>
      <div className="flex-1 mr-24 flex items-center">
        <div className="flex flex-col lg:ml-15">
          <h1 className="text-6xl leading-[70px] font-bold">{props.title}</h1>
          <p className="leading-8 mt-6">{props.description}</p>
          <div className="flex items-center mt-8">
            <div className="flex flex-col font-bold gap-3">
              <span>Genre : {props.genres.join(", ")}</span>
              <span>Duration : {durationStr}</span>
            </div>
            <button className="border-2 py-2 px-8 ml-24 rounded-sm btn-hero group transition-all hover:scale-105">
              <div
                onClick={handleBuyTicket}
                className="group-hover:text-primary-dark"
              >
                Buy Ticket
              </div>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSlide;
