import React, { ForwardedRef, forwardRef, RefObject } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Parallax, Autoplay } from "swiper";
import HeroSlide from "./HeroSlide";
import { Element } from "react-scroll";
import { Movie } from "../../types/movie";

type Props = {
  movies: Movie[];
};

const HeroSlider = ({ movies }: Props, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <Element name="home" className="element">
      <div ref={ref} className="h-screen w-full">
        <Swiper
          pagination={{
            clickable: true,
          }}
          loop={true}
          modules={[Pagination, Parallax, Autoplay]}
          className="h-full w-full swiper-style"
        >
          {movies.map((movie) => (
            <SwiperSlide key={movie.id}>
              <HeroSlide
                urlId={movie.urlId || ""}
                title={movie.title}
                coverUrl={movie.coverUrl}
                description={movie.description}
                genres={movie.genres}
                duration={movie.duration}
              />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </Element>
  );
};

export default forwardRef(HeroSlider);
