import React, { memo } from "react";

type Props = {
  date: number;
  className?: string;
  onClick?: React.MouseEventHandler;
  active?: boolean;
};

const options: Intl.DateTimeFormatOptions = {
  month: "short",
  day: "numeric",
};

const ScheduleButton = ({ date, className, active, onClick }: Props) => {
  // get date in week
  const newDate = new Date(date);
  const day = Intl.DateTimeFormat("en-US", options).format(newDate);

  const weekday = Intl.DateTimeFormat("en-US", { weekday: "short" }).format(
    newDate
  );
  return (
    <div
      onClick={onClick}
      className={`flex flex-col w-[80px] items-center gap-1 py-4 rounded-lg cursor-pointer select-none ${
        className ? className : ""
      } ${active ? "bg-secondary-yellow" : "bg-gray-dark text-gray-light"}`}
    >
      <span className="uppercase leading-none text-sm font-medium">{day}</span>
      <span className="uppercase font-bold text-3xl leading-none tracking-tighter">
        {weekday}
      </span>
    </div>
  );
};

export default memo(ScheduleButton);
