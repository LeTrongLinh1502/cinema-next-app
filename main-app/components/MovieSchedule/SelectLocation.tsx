import React, { memo, SetStateAction, useRef } from "react";

type Region = {
  name: string;
  id: string;
};

const regions: Region[] = [
  {
    id: "hanoi",
    name: "Hà Nội",
  },
  {
    id: "hochiminh",
    name: "Hồ Chí Minh",
  },
  {
    id: "haiphong",
    name: "Hải Phòng",
  },
  {
    id: "cantho",
    name: "Cần Thơ",
  },
  {
    id: "danang",
    name: "Đà Nẵng",
  },
  {
    id: "quangninh",
    name: "Quảng Ninh",
  },
];

const isRegionInList = (regionId: string) => {
  return regions.some((region) => region.id === regionId);
};

type Props = {
  selectedRegion: Region | undefined;
  onChange: React.Dispatch<SetStateAction<Region | undefined>>;
};

const SelectLocation = ({ selectedRegion, onChange }: Props) => {
  const selectRef = useRef<HTMLSelectElement>(null);

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange({
      id: e.target.value,
      name: e.target.options[e.target.selectedIndex].label,
    });
  };

  return (
    <div className="form-control w-full max-w-xs">
      <select
        ref={selectRef}
        className="select text-base bg-gray-dark"
        onChange={handleChange}
        value={selectedRegion ? selectedRegion.id : ""}
      >
        <option disabled value="" defaultChecked>
          Or choose your location
        </option>
        {selectedRegion && !isRegionInList(selectedRegion.id) && (
          <option
            key={selectedRegion.name}
            value={selectedRegion.id}
            label={selectedRegion.name}
          />
        )}
        {regions.map((region) => (
          <option key={region.id} value={region.id} label={region.name} />
        ))}
      </select>
    </div>
  );
};

export default memo(SelectLocation);
