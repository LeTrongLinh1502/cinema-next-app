import React from "react";
import Plus from "/assets/MovieDetail/Plus.svg";
import Check from "/assets/MovieDetail/Check.svg";
type Props = {
  className?: string;
  inWatchList: boolean;
};

const ButtonWatchList = ({ className, inWatchList }: Props) => {
  return (
    <button
      className={`border-2 py-2 px-8 rounded-sm btn-hero group transition-all hover:scale-105 ${
        className ? className : ""
      }`}
    >
      <div className="group-hover:text-primary-dark flex items-center gap-2 font-bold z-10">
        {inWatchList ? (
          <>
            <Check width="24" height="24" stroke-width="0" />
            <span>In Watchlist</span>
          </>
        ) : (
          <>
            <Plus width="18" height="18" />
            <span>Add to Watchlist</span>
          </>
        )}
      </div>
    </button>
  );
};

export default ButtonWatchList;
