import Image from "next/image";
import React, { useMemo, useRef } from "react";
import ReactPlayer from "react-player/youtube";
import PlayButton from "../Utils/PlayButton";
import MovieDetailHeader from "./MovieDetailHeader";
import { Movie } from "../../types/movie";
import ButtonWatchList from "./ButtonWatchList";
import TrailerCover from "./TrailerCover";
import { ratingConverter } from "@/utils/firebaseConverter/firebaseConverter";
import { useFirestoreDocumentData } from "@react-query-firebase/firestore";
import { doc } from "firebase/firestore";
import { db } from "firebaseConfig";

interface Props extends Movie {}

const MovieDetail = (props: Props) => {
  const playerRef = useRef<ReactPlayer>(null);
  const { coverUrl } = props;
  const wrapper = useMemo(() => TrailerCover(coverUrl), [coverUrl]);
  const durationString = useMemo(() => {
    return `${Math.floor(props.duration / 60)}h ${props.duration % 60}m`;
  }, [props.duration]);

  const ratingData = useFirestoreDocumentData(
    ["ratings", props.id],

    doc(db, props.ratingPath).withConverter(ratingConverter),
    {
      subscribe: true,
    }
  );

  return (
    <div className="w-full flex flex-col gap-4">
      <MovieDetailHeader
        id={props.id}
        title={props.title}
        duration={durationString}
        rating={ratingData.data?.rating || 5.0}
        ageRating={props.ageRating}
        releaseDate={props.releaseDate}
        votes={ratingData.data?.votes || 0}
        yourRating={undefined}
        status={props.status}
      />
      <div className="flex h-[500px]">
        <div className="w-full max-w-xs relative shadow-light-thin rounded-md overflow-hidden">
          <Image
            src={props.imageUrl}
            alt="poster"
            layout="fill"
            objectFit="cover"
            priority={true}
          />
        </div>
        <div className="ml-3 flex-1 shadow-light-thin rounded-md overflow-hidden react-player-wrapper">
          <ReactPlayer
            className="react-player"
            url={props.trailerUrl}
            light={"#"}
            width="100%"
            height="100%"
            playing={true}
            controls={true}
            ref={playerRef}
            wrapper={wrapper}
            config={{
              playerVars: {
                disablekb: 1,
                color: "white",
              },
            }}
            playIcon={PlayButton({
              className: "lg:w-40 lg:h-40 text-white opacity-70",
            })}
            onEnded={() => {
              playerRef.current?.showPreview();
            }}
          />
        </div>
      </div>
      <div className="flex mt-4">
        <div className="flex flex-col gap-6 basis-2/3">
          <ul className="flex gap-3">
            {props.genres.map((genre, idx) => (
              <li
                key={idx}
                className="block py-1 px-3 rounded-full border-2 border-white cursor-pointer hover:bg-title-dark"
              >
                {genre}
              </li>
            ))}
          </ul>
          <p className="text-[17px]">{props.description}</p>
        </div>
        <div className="flex-1 flex justify-end items-start">
          <ButtonWatchList inWatchList={false} />
        </div>
      </div>
    </div>
  );
};

export default MovieDetail;
