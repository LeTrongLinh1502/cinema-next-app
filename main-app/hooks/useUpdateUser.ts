import { MyUserData } from "@/types/profile";
import { useAuthUpdateProfile, useAuthUser } from "@react-query-firebase/auth";
import { useFirestoreDocumentMutation } from "@react-query-firebase/firestore";
import {
  Auth,
  AuthError,
  updatePhoneNumber,
  updateProfile,
  User,
} from "firebase/auth";
import { doc, FirestoreError, setDoc } from "firebase/firestore";
import { auth, db } from "firebaseConfig";
import Error from "next/error";
import { useEffect, useMemo, useState } from "react";
import useAuth from "./useAuth";

type NewUser = {
  displayName?: string;
  address?: string;
  phone?: string;
};

type Options = {
  onSuccess?: () => void;
  onError?: () => void;
};

const useUpdateUser = (auth: Auth, options?: Options) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<AuthError | FirestoreError | undefined>();
  const [isError, setIsError] = useState(false);
  const user = auth.currentUser;
  const uid = user?.uid;
  const ref = useMemo(() => doc(db, "users", uid || "1"), [uid]);

  const mutate = async (newUser: NewUser) => {
    if (!user) return;
    try {
      setIsLoading(true);
      setIsError(false);
      const { address, phone, displayName } = newUser;
      await setDoc(
        ref,
        {
          address,
          phone,
          updatedAt: Date.now(),
        },
        { merge: true }
      );
      await updateProfile(user, {
        displayName,
      });
      if (options?.onSuccess) options.onSuccess();
    } catch (err) {
      setIsError(true);
      if (err instanceof FirestoreError) {
        setError(err);
      }
      if ((err as AuthError).code.includes("auth")) {
        setError(err as AuthError);
      }

      if (options?.onError) options.onError();
    } finally {
      setIsLoading(false);
    }
  };
  return { isLoading, error, isError, mutate };
};

export default useUpdateUser;
