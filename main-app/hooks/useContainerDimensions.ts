import React, { useState, useEffect } from "react";

export function useContainerDimensions(myRef: React.RefObject<any>) {
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

  useEffect(() => {
    const getDimensions = () => ({
      width: (myRef && myRef.current.offsetWidth) || 0,
      height: (myRef && myRef.current.offsetHeight) || 0,
    });

    if (myRef.current) {
      setDimensions(getDimensions());
    }
  }, [myRef]);
  return dimensions;
}
