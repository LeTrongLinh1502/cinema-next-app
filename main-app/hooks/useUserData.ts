import { UserData } from "@/types/profile";
import { userConverter } from "@/utils/firebaseConverter/firebaseConverter";
import { useFirestoreDocumentData } from "@react-query-firebase/firestore";
import { Auth } from "firebase/auth";
import { doc } from "firebase/firestore";
import { db } from "firebaseConfig";
import { useMemo } from "react";

const useUserData = (auth: Auth) => {
  const uid = auth.currentUser?.uid;
  const ref = useMemo(
    () => doc(db, "users", uid || "0").withConverter<UserData>(userConverter),
    [uid]
  );

  const userDb = useFirestoreDocumentData<UserData>("user", ref, undefined, {
    enabled: !!uid,
  });
  return userDb;
};

export default useUserData;
