import { object } from "prop-types";

export function formatRegionId(regionName: string) {
  const regionId = regionName
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/\s/g, "")
    .toLowerCase();
  return regionId;
}

/**
 * @param {string} keyName - A key to identify the value.
 * @param {any} keyValue - A value associated with the key.
 * @param {number} ttl- Time to live in seconds.
 */
export const storeWithExpiration = (
  keyName: string,
  keyValue: any,
  ttl: number
) => {
  const data = {
    value: keyValue, // store the value within this object
    ttl: Date.now() + ttl * 1000, // store the TTL (time to live)
  };

  // store data in LocalStorage
  localStorage.setItem(keyName, JSON.stringify(data));
};

/**
 * @param {string} keyName - A key to identify the data.
 * @returns {any|null} returns the value associated with the key if its exists and is not expired. Returns `null` otherwise
 */
export const getWithExpiration = (keyName: string): any | null => {
  const data = localStorage.getItem(keyName);
  if (!data) {
    // if no value exists associated with the key, return null
    return null;
  }

  const item = JSON.parse(data);

  // If TTL has expired, remove the item from localStorage and return null
  if (Date.now() > item.ttl) {
    localStorage.removeItem(keyName);
    return null;
  }

  // return data if not expired
  return item.value;
};

interface Coordinates {
  latitude: number;
  longitude: number;
}

// find km between two location
const deg2rad = (deg: number) => {
  return deg * (Math.PI / 180);
};

export const getDistanceFromLatLonInKm = (
  cor1: Coordinates,
  cor2: Coordinates
) => {
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(cor2.latitude - cor1.latitude); // deg2rad below
  const dLon = deg2rad(cor2.longitude - cor1.longitude);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(cor1.latitude)) *
      Math.cos(deg2rad(cor2.latitude)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return d;
};

export function isMobile() {
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    return true;
  } else {
    return false;
  }
}

export function objectNotEmpty(obj: any) {
  if (obj === null || obj === undefined) {
    return false;
  }
  return Object.keys(obj).length > 0;
}

export const groupBy = <T, K extends keyof any>(
  list: T[],
  getKey: (item: T) => K
) =>
  list.reduce((previous, currentItem) => {
    const group = getKey(currentItem);
    if (!previous[group]) previous[group] = [];
    previous[group].push(currentItem);
    return previous;
  }, {} as Record<K, T[]>);

export const formatTimeString = (date: Date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const timeString = `${hours}:${String(minutes).padStart(2, "0")}`;
  if (hours > 12) {
    return `${timeString} PM`;
  }
  return `${timeString} AM`;
};

export const getDayName = (timestamp: number, locale: string) => {
  const date = new Date(timestamp);
  return date.toLocaleDateString(locale, { weekday: "long" });
};
export const getMonthAndDayStr = (timestamp: number, locale: string) => {
  const date = new Date(timestamp);
  return date.toLocaleDateString(locale, { month: "long", day: "numeric" });
};
