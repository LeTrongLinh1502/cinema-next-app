import { Movie, Rating } from "@/types/movie";
import { Cinema, Room, Schedule } from "@/types/movieBooking";
import { UserData } from "@/types/profile";
import {
  doc,
  DocumentData,
  QueryDocumentSnapshot,
  SnapshotOptions,
} from "firebase/firestore";
import { db } from "firebaseConfig";

export const userConverter = {
  toFirestore: (userData: UserData) => {
    return {
      uid: userData.uid,
      createdAt: userData.createdAt,
      updatedAt: userData.updatedAt,
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): UserData => {
    const data = snapshot.data()!;
    return {
      uid: data.uid,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      address: data.address,
      phone: data.phone,
    };
  },
};

export const moviesConverter = {
  toFirestore: (movie: Movie) => {
    const { ratingPath, ...movieData } = movie;
    return {
      ...movieData,
      ratingRef: doc(db, ratingPath),
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): Movie => {
    const data = snapshot.data()!;
    const { ratingRef, genres, ...movieData } = data;
    return {
      id: snapshot.id,
      ...movieData,
      ratingPath: ratingRef.path,
      genres: data.genres.split(","),
    } as Movie;
  },
};

export const ratingConverter = {
  toFirestore: (rating: Rating) => {
    return {
      rating: rating.rating,
      votes: rating.votes,
      movieRef: doc(db, rating.moviePath),
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): Rating => {
    const data = snapshot.data()!;
    return {
      rating: data.rating,
      votes: data.votes,
      moviePath: data.movieRef.path,
    };
  },
};

export const scheduleConverter = {
  toFirestore: (schedule: Schedule) => {
    return {
      ...schedule,
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): Schedule => {
    const data = snapshot.data()!;
    return {
      ...(data as Schedule),
    };
  },
};

export const cinemaConverter = {
  toFirestore: (cinema: Cinema) => {
    return {
      ...cinema,
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): Cinema => {
    const data = snapshot.data()!;
    return {
      ...(data as Cinema),
      id: snapshot.id,
    };
  },
};

export const roomConverter = {
  toFirestore: (room: Room) => {
    return {
      ...room,
    };
  },
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions | undefined
  ): Room => {
    const data = snapshot.data()!;
    return {
      ...(data as Room),
    };
  },
};
