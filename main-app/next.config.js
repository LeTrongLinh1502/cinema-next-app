/** @type {import('next').NextConfig} */
const { i18n } = require("./next-i18next.config");
const nextConfig = {
  reactStrictMode: true,
  i18n,
  images: {
    domains: [
      "api.lorem.space",
      "images4.alphacoders.com",
      "images2.alphacoders.com",
      "images.unsplash.com",
      "wall.alphacoders.com",
      "images8.alphacoders.com",
      "images5.alphacoders.com",
      "lh3.googleusercontent.com",
      "graph.facebook.com",
      "firebasestorage.googleapis.com",
    ],
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
};

module.exports = nextConfig;
