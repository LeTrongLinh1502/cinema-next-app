import { DocumentReference } from "firebase/firestore";
import { StorageReference } from "firebase/storage";

type MovieStatus = "noschedule" | "nowshowing" | "upcoming";

export interface Movie {
  id: string;
  title: string;
  imageUrl: string;
  genres: string[];
  duration: number;
  description: string;
  trailerUrl: string;
  coverUrl: string;
  ageRating: string;
  releaseDate: string;
  ratingPath: string;
  status: MovieStatus;
  urlId?: string;
}

export type Rating = {
  rating: number;
  votes: number;
  moviePath: string;
};
