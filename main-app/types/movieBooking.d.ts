export type Status = "AVAILABLE" | "RESERVED" | "UNAVAILABLE";

export type SeatType = {
  name: string;
  id: string;
  price: number;
  color?: string;
};

export type Seat = {
  id: string;
  type: SeatType;
  status: Status;
  price?: number;
};

export type HourSchedule = {
  endTimestamp: number;
  startTimestamp: number;
  room: string;
  roomShiftPath: string;
  showType: string;
};

export type CinemaSchedule = {
  id: string;
  name: string;
  hourSchedules: hourSchedule[];
};

export type Schedule = {
  movieId: string;
  timestamp: number;
  region: string;
  cinemaSchedules: CinemaSchedule[];
};

export type Room = {
  name: string;
  rowSize: number;
  seatData: Seat[];
};

export type Cinema = {
  id: string;
  name: string;
  address: string;
  region: string;
  location: string;
};
