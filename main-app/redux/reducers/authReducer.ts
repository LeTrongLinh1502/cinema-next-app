import { createSlice } from "@reduxjs/toolkit";

type AuthState = {
  isAuth: boolean;
  authMethod: string | undefined;
  uid: string | undefined;
};

const initialState: AuthState = {
  isAuth: false,
  authMethod: undefined,
  uid: undefined,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    login(state, action) {
      state.isAuth = true;
      state.authMethod = action.payload.authMethod;
      state.uid = action.payload.uid;
    },

    logout(state) {
      state.isAuth = false;
      state.authMethod = undefined;
      state.uid = undefined;
    },
  },
});
export const authActions = authSlice.actions;
export default authSlice.reducer;
