import Register from "../../components/Auth/Register";
import PageWrapper from "../../components/Utils/PageWrapper";

const SignUpPage = () => {
  return (
    <PageWrapper>
      <Register />
    </PageWrapper>
  );
};

export default SignUpPage;
