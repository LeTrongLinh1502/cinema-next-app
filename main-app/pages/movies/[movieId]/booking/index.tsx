import { moviesConverter } from "@/utils/firebaseConverter/firebaseConverter";
import { collection, getDocs, limit, query, where } from "firebase/firestore";
import { db } from "firebaseConfig";
import {
  GetStaticPaths,
  GetStaticPropsContext,
  InferGetStaticPropsType,
  NextPage,
} from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Layout from "../../../../components/Layout/Layout";
import MovieBooking from "../../../../components/MovieBooking/MovieBooking";

const BookingMoviePage = ({
  movie,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  const { t } = useTranslation();
  return (
    <Layout t={t}>
      <MovieBooking movie={movie} />
    </Layout>
  );
};

export default BookingMoviePage;

export const getStaticProps = async (ctx: GetStaticPropsContext) => {
  const movieId = ctx.params?.movieId as string;
  const movieQuery = query(
    collection(db, "movies"),
    where("urlId", "==", movieId),
    limit(1)
  ).withConverter(moviesConverter);
  const querySnapshot = await getDocs(movieQuery);
  const movie = querySnapshot.docs[0].data();
  return {
    props: {
      ...(await serverSideTranslations(ctx.locale as string, [
        "nav",
        "default",
      ])),
      movie: movie || null,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const moviesRef = collection(db, "movies");
  const moviesQuery = query(moviesRef).withConverter(moviesConverter);
  const moviesQuerySnapshot = await getDocs(moviesQuery);
  const movies = moviesQuerySnapshot.docs.map((doc) => doc.data());
  const paths = movies.map((movie) => ({
    params: {
      movieId: movie.urlId,
    },
  }));
  return {
    paths,
    fallback: false,
  };
};
