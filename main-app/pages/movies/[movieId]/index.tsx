import { moviesConverter } from "@/utils/firebaseConverter/firebaseConverter";
import {
  collection,
  doc,
  getDoc,
  getDocs,
  limit,
  query,
  where,
} from "firebase/firestore";
import { db } from "firebaseConfig";
import {
  GetStaticPaths,
  GetStaticProps,
  GetStaticPropsContext,
  InferGetStaticPropsType,
} from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import Layout from "../../../components/Layout/Layout";
import MovieDetail from "../../../components/MovieDetail/MovieDetail";
import MovieSchedule from "../../../components/MovieSchedule/MovieSchedule";

const MovieDetailPage = ({
  movie,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  const { t } = useTranslation();
  const router = useRouter();
  if (!movie) {
    router.replace("/");
    return;
  }

  return (
    <Layout t={t}>
      <div className="max-w-7xl mx-auto py-8 flex flex-col gap-8">
        <MovieDetail
          id={movie.id}
          title={movie.title}
          imageUrl={movie.imageUrl}
          description={movie.description}
          genres={movie.genres}
          duration={movie.duration}
          ageRating={movie.ageRating}
          releaseDate={movie.releaseDate}
          trailerUrl={movie.trailerUrl}
          coverUrl={movie.coverUrl}
          ratingPath={movie.ratingPath}
          status={movie.status}
        />
        {movie.status === "nowshowing" && <MovieSchedule movieId={movie.id} />}
      </div>
    </Layout>
  );
};

export default MovieDetailPage;

export const getStaticProps = async (ctx: GetStaticPropsContext) => {
  const movieId = ctx.params?.movieId as string;
  const movieQuery = query(
    collection(db, "movies"),
    where("urlId", "==", movieId),
    limit(1)
  ).withConverter(moviesConverter);
  const querySnapshot = await getDocs(movieQuery);
  const movie = querySnapshot.docs[0].data();

  return {
    props: {
      ...(await serverSideTranslations(ctx.locale as string, [
        "nav",
        "default",
      ])),
      movie: movie || null,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const moviesRef = collection(db, "movies");
  const moviesQuery = query(moviesRef).withConverter(moviesConverter);
  const moviesQuerySnapshot = await getDocs(moviesQuery);
  const movies = moviesQuerySnapshot.docs.map((doc) => doc.data());
  const paths = movies.map((movie) => ({
    params: {
      movieId: movie.urlId,
    },
  }));
  return {
    paths,
    fallback: false,
  };
};
